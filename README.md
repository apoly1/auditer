# Auditer

## What is it?

Auditer is a CI tool to easily run "Composer Audit" on your project with more information and configuration.
It consults the [CVE database](https://www.cve.org/) to get more info and most importantly the severity level.
It also allows you to configure the minimum severity level to trigger an error.

## How to use it?

Add this to your pipeline:

```gitlab-ci.yml
test:auditer:
  stage: test
  image: profideo/auditer:lastest
  services:
    - docker:dind
  script:
    - docker run -v /var/run/docker.sock:/var/run/docker.sock -v /var/www/symfony:/var/www/symfony $IMAGE_TEST_NAME
    - php index.php auditer /var/www/symfony -m=$MINIMUM_SEVERITY
  variables:
    GIT_STRATEGY: none
  tags:
    - docker
  only:
    - branches
  artifacts:
    paths:
      - ./var/report.html
    expire_in: 1 week
  except:
    refs:
      - tags
      - master
      - develop

```
You will need to define $IMAGE_TEST_NAME after the name of the docker image used by your CI.

## How to run it locally?

Clone the project, and run ``docker compose up``. This will install the project dependancy.
If you want to test it on a specific project, you will need to edit the ``docker-compose.yml`` file and add a volume to your project.

Example (if I cloned this project into 'auditer' and I want to test the project customer-file-workflow-api):

```
  app:
    volumes:
      [...]
      - ../customer-file-workflow-api:/var/www/example1:cfwa
```

Then inside the auditer container:

```bash
php index.php audit /var/www/cfwa/
```
This will run the audit and generate the report in var/ if needed