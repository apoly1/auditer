<?php

require __DIR__ . '/vendor/autoload.php';

use App\Commands\AuditCommand;
use Symfony\Component\Console\Application;

$app = new Application();
$app->add(new AuditCommand());

$app->run();