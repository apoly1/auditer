<?php

namespace App;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class SeverityParser
{
    public function __construct(private HttpClientInterface $client)
    {
    }

    //todo return a data object instead of an array
    public function parse(array $advisories): array
    {
        $severities = [];
        /**
         * @var string $packageName
         * @var array[] $errors
         */
        foreach ($advisories as $packageName => $errors) {
            foreach ($errors as $error) {
                $cveId = $error['cve'] ?? null;
                if ($cveId === null) {
                    continue;
                }
                $advisory = $this->getSecurityLevelFromCVEAPI($error['cve']);
                if ($advisory === null) {
                    continue;
                }
                if (!isset($severities[$advisory['severity']])) {
                    $severities[$advisory['severity']] = [];
                }
                $severities[$advisory['severity']][] = $advisory;
            }
        }

        return $severities;
    }

    private function getSecurityLevelFromCVEAPI(string $cveID): ?array
    {
        $response = $this->client->request('GET', 'https://cveawg.mitre.org/api/cve/'.$cveID);

        $arrayResponse = $response->toArray();
        if (!isset($arrayResponse['containers']['cna'])) {
            return null;
        }

        $cna = $arrayResponse['containers']['cna'];
        $references = $cna['references'];
        $affected = $cna['affected'];
        $descriptions = $cna['descriptions'];
        $severity = $cna['metrics'][0]['cvssV3_1']['baseSeverity'];
        $githubLink = $cna['references'][0]['url'];
        return [
            'cveID' => $cveID,
            'title' => $cna['title'],
            'severity' => $severity,
            'githubLink' => $githubLink,
            'more' => [
                'references' => $references,
                'affected' => $affected,
                'descriptions' => $descriptions
            ]
        ];
    }

}