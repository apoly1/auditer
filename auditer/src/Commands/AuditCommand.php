<?php

namespace App\Commands;

use App\ComposerAuditer;
use App\ReportGenerator;
use App\SeverityChecker;
use App\SeverityParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

class AuditCommand extends Command
{
    private ComposerAuditer $composerAuditer;
    private SeverityParser $severityParser;
    private ReportGenerator $reportGenerator;
    private SeverityChecker $severityChecker;

    public function __construct(string $name = null)
    {
        parent::__construct($name);
        $client = HttpClient::create();
        $this->composerAuditer = new ComposerAuditer();
        $this->severityParser = new SeverityParser($client);
        $this->reportGenerator = new ReportGenerator();
        $this->severityChecker = new SeverityChecker();
    }

    protected static $defaultDescription = 'Run the audit.';

    protected function configure(): void
    {
        $this
            ->setName('audit')
            ->addArgument(
                'directoryPath',
                InputArgument::REQUIRED,
                'the absolute path to the directory where the composer file is.'
            )
            ->addOption(
                'minimumSeverity',
                'm',
                InputArgument::OPTIONAL,
                'The minimum severity level for throwing an error.',
                'CRITICAL'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $directoryPath */
        $directoryPath = $input->getArgument('directoryPath');
        /** @var string $minimumSeverity */
        $minimumSeverity = $input->getOption('minimumSeverity');
        $output->writeln("Checking for vulnerabilities in file $directoryPath with minimum severity '$minimumSeverity'.");
        $advisories = $this->composerAuditer->run($directoryPath);
        if ($advisories === null) {
            $output->writeln("Could not find a composer file to scan.");
            return Command::FAILURE;
        }
        $severities = $this->severityParser->parse($advisories);

        if ($severities !== []) {
            $output->writeln(count($severities) . " severities found, generating report...");
            $this->reportGenerator->createReport($severities);
        }

        if ($this->severityChecker->checkSeverityIsGreaterThanMinimum(array_keys($severities), $minimumSeverity)) {
            $output->writeln("Warning: A critical severity was spotted!");
            return Command::FAILURE;
        }

        $output->writeln("No critical breach was detected.");
        return Command::SUCCESS;
    }

}