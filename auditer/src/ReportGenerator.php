<?php

namespace App;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class ReportGenerator
{
    private Environment $twig;
    private \DateTimeImmutable $now;

    public function __construct()
    {
        $loader = new FilesystemLoader(__DIR__ . '/../templates');
        $this->twig = new Environment($loader);
        $this->now = new \DateTimeImmutable();
    }

    public function createReport(array $severities): void
    {
        $template = $this->twig->load('template.html.twig');
        $html = $template->render([
            'severities' => $severities,
            'now' => $this->now,
        ]);

        file_put_contents(__DIR__ . '/../var/report.html', $html);
    }
}