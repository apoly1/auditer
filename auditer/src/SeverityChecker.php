<?php

namespace App;

class SeverityChecker
{
    private const SEVERITY_LEVELS = [
        'CRITICAL',
        'HIGH',
        'MEDIUM',
        'LOW',
        'NONE'
    ];

    /**
     * @param string[] $severities
     */
    public function checkSeverityIsGreaterThanMinimum(array $severities, string $minimumSeverity): bool
    {
        //find critical severities
        $minimumSeverityIndex = array_search($minimumSeverity, self::SEVERITY_LEVELS);
        if ($minimumSeverityIndex === false) {
            throw new \InvalidArgumentException('Invalid severity');
        }
        $criticalSeverities = array_slice(self::SEVERITY_LEVELS, 0, $minimumSeverityIndex + 1);
        //check if collusion of critical severities and severities
        return count(array_intersect($severities, $criticalSeverities)) > 0;

    }

}