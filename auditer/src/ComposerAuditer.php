<?php

namespace App;

use RuntimeException;

class ComposerAuditer
{
    public function run(string $directoryPath): ?array
    {
        $output = [];
        exec("cd $directoryPath && composer audit --format=json", $output);

        if ($output === []) {
            return null;
        }
        $arrayOutput = $this->parseAuditOutputIntoArray($output);
        if (!isset($arrayOutput['advisories'])) {
            throw new RuntimeException('Incorrect output');
        }
        return $arrayOutput['advisories'];
    }


    /**
     * Parse the output of composer audit into an associative array
     */
    private function parseAuditOutputIntoArray(array $output): array
    {
        $json = implode("\n", $output);
        /** @var array $array */
        $array  = json_decode($json, true);
        return $array;
    }

}