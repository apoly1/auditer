<?php

namespace tests;

use App\SeverityChecker;
use PHPUnit\Framework\TestCase;

class SeverityCheckerTest extends TestCase
{
    protected function setUp(): void
    {
        $this->severityChecker = new SeverityChecker();
    }

    public function testIncorrectSeverity(): void
    {
        $severities = [
            'LOW',
            'MEDIUM',
            'HIGH',
        ];

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid severity');
        $this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'INCORRECT');
    }

    public function testSeverityIsOnlyHigh(): void
    {
        $severities = [
            'HIGH',
        ];

        $this->assertTrue($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'HIGH'));
        $this->assertTrue($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'MEDIUM'));
        $this->assertTrue($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'LOW'));
    }

    public function testSeverityIsOnlyMedium(): void
    {
        $severities = [
            'MEDIUM',
        ];

        $this->assertFalse($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'HIGH'));
        $this->assertTrue($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'MEDIUM'));
        $this->assertTrue($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'LOW'));
    }

    public function testSeverityIsOnlyLow(): void
    {
        $severities = [
            'LOW',
        ];

        $this->assertFalse($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'HIGH'));
        $this->assertFalse($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'MEDIUM'));
        $this->assertTrue($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'LOW'));
    }

    public function testSeveritiesAreMoreThanLow(): void
    {
        $severities = [
            'MEDIUM',
            'HIGH',
        ];

        $this->assertTrue($this->severityChecker->checkSeverityIsGreaterThanMinimum($severities, 'LOW'));
    }

}