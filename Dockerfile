FROM thecodingmachine/php:8.1-v4-apache

ENV MINIMUM_SEVERITY=HIGH
ENV APP_ENV=prod

COPY auditer /var/www/html

WORKDIR /var/www/html

RUN composer install --no-dev --no-scripts --no-progress --no-suggest --optimize-autoloader --no-interaction --no-cache --prefer-dist

#todo: remove other dev files
RUN rm -rf ./tests

##todo install docker

#TODO: run the audit command on the target project
CMD php index.php auditer /var/www/symfony -m=$MINIMUM_SEVERITY
